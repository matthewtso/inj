extern crate handlebars;
#[macro_use]
extern crate serde_json;

use handlebars::Handlebars;
use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::io::{self, BufRead};

///
/// Usage:
///
///     cat [path/to/handlebars.template] | env $(cat .env | xargs) inj >> [path/to/output.file]
///
/// Installation:
///
///     git clone [repo url]
///     cd inj
///     cargo build --release
///     mv target/release/inj /usr/local/bin/
///
fn main() -> Result<(), Box<Error>> {
    let stdin = io::stdin();

    let input: Vec<String> = stdin
        .lock()
        .lines()
        .filter(|line| if let Ok(_) = line { true } else { false })
        .map(|line| line.unwrap())
        .collect();

    let vars = env::vars().fold(HashMap::new(), |mut acc, var| {
        let (k, v) = var;
        acc.insert(k, v);
        acc
    });

    let input = input.join("\n");

    let reg = Handlebars::new();

    print!("{}", reg.render_template(&input, &json!(vars))?);

    Ok(())
}
